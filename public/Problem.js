
const csv = require('csv-parser');
const fs = require('fs');

const res = [0, 0, 0, 0, 0];
const fulldate = {};
const principal = {};
const aggrigate = {};
let temp;

fs.createReadStream('test.csv')
  .pipe(csv())
  .on('data', (line) => {
    // -----------Problem 1-----------------------------
    if (parseInt(line.AUTHORIZED_CAP, 0) < 100000) {
      res[0] += 1;
    }
    if (parseInt(line.AUTHORIZED_CAP, 0) >= 100000
        && parseInt(line.AUTHORIZED_CAP, 0) < 1000000) {
      res[1] += 1;
    }
    if (parseInt(line.AUTHORIZED_CAP, 0) >= 1000000
        && parseInt(line.AUTHORIZED_CAP, 0) < 10000000) {
      res[2] += 1;
    }
    if (parseInt(line.AUTHORIZED_CAP, 0) >= 100000000
        && parseInt(line.AUTHORIZED_CAP, 0) < 1000000000) {
      res[3] += 1;
    }
    if (parseInt(line.AUTHORIZED_CAP, 0) >= 1000000000) {
      res[4] += 1;
    }

    // -----------Problem 2-----------------------------
    const dmy = line.DATE_OF_REGISTRATION.split('-');
    const year = dmy[2];
    if (year > 2000 && year <= 2018) {
      if (!fulldate[year]) {
        fulldate[year] = 0;
      }
      fulldate[year] += 1;
    }

    // -----------Problem 3-----------------------------
    if (year === '2015') {
      const company = line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN;
      if (!principal[company]) {
        principal[company] = 0;
      }
      principal[company] += 1;
    }

    // -----------Problem 4-----------------------------
    if (year >= 2010 && year <= 2018) {
      if (!aggrigate[line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN]) {
        aggrigate[line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN] = {};
        temp = aggrigate[line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN];
        if (!temp[year]) { temp[year] = 1; }
      } else {
        temp = aggrigate[line.PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN];
        if (!temp[year]) { temp[year] = 1; } else { temp[year] += 1; }
      }
    }
  })
  .on('end', () => {
    fs.writeFile('result.json', JSON.stringify(res), () => {});
    fs.writeFile('runs.json', JSON.stringify(fulldate), () => {});
    fs.writeFile('range.json', JSON.stringify(principal), () => {});
    fs.writeFile('agg.json', JSON.stringify(aggrigate), () => {});
  });
