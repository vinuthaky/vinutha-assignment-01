fetch('agg.json')
  .then(resp => resp.json())
  .then((data) => {
    const year = [2010];
    for (let i = 0; year[i] < 2019; i += 1) {
      year.push(year[i] + 1);
    }
    const temparr = [];
    const pba = Object.keys(data);
    for (let i = 0; i < pba.length; i += 1) {
      temparr.push(
        {
          name: pba[i],
          data: Object.values(data[pba[i]]),
        },
      );
    }
    Highcharts.chart('container3', {
      chart: {
        type: 'bar',
      },
      title: {
        text: 'Stacked-bar-chart',
      },
      xAxis: {
        categories: year,
      },
      yAxis: {
        title: {
          text: 'Company Registered On that Respective Year',
        },
      },
      plotOptions: {
        series: {
          stacking: 'normal',
        },
      },
      series: temparr,
    });
  });
