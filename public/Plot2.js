fetch('runs.json')
  .then(resp => resp.json())
  .then((data) => {
    // eslint-disable-next-line no-undef
    const key = Object.keys(data);
    const value = Object.values(data);

    Highcharts.chart('container1', {
      chart: {
        type: 'column',
      },
      title: {
        text: ' Company Registered Based On Year',
      },
      xAxis: {

        type: 'category',
        categories: key,
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: ' Count',
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: 'Number of companies have Capital: <b>{point.y:.1f} millions</b>',
      },
      series: [{
        name: 'Population',
        data: value,
        colorByPoint: true,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#FFFFFF',
          align: 'right',
          format: '{point.y:.1f}',
          y: 10,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
        },
      }],
    });
  });
